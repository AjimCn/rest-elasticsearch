package com.example.elastic.repository;

import com.example.elastic.pojo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface PojoRepository extends ElasticsearchRepository<pojo,String> {
    List<pojo> findByDogs_Name(String name);
    Long countByDogsName(String name);
    Long countByDogsNameEquals(String name);

    //给文档设置好状态,发生更新之后将其设置成

}
