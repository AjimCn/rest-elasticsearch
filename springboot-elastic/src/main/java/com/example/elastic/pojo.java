package com.example.elastic;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;

@Document(indexName = "document")
@Setter
@Getter
public class pojo {
    @Field(type = FieldType.Auto)
    private List<Dog> dogs;

    @Id
    private String id;

}
