package com.example.elastic.contoller;

import com.example.elastic.Dog;
import com.example.elastic.pojo;
import com.example.elastic.repository.PojoRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@RestController
public class PojoController {
    @Resource
    private PojoRepository pojoRepository;
    @GetMapping("hello")
    public void save(){
        pojo pojo = new pojo();
        Dog dog = new Dog();
        dog.setName("小黄");
        pojo.setId("2");
        pojo.setDogs(Arrays.asList(dog));
        pojoRepository.save(pojo);
    }
    @GetMapping("word")
    public pojo get(){
        List<pojo> dogsName = pojoRepository.findByDogs_Name("阿黄");
        return dogsName.get(0);
    }

    @GetMapping("xx")
    public Long getCount(){
        return pojoRepository.countByDogsName("黄");
    }
    @GetMapping("oo")
    public Long getaCount(){
        return pojoRepository.countByDogsNameEquals("阿黄");
    }

}
