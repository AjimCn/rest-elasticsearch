package com.example.elastic;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Setter
@Getter
public class Dog {
    @Field(type = FieldType.Text,analyzer = "ik_max_word")
    private String name;
    private Long id;
}
