package com.ajim.springelasticsearch;

import com.ajim.springelasticsearch.pojo.News;
import com.alibaba.fastjson.JSON;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class SpringElasticsearchApplicationTests {
    @Autowired
    @Qualifier("restHighLevelClient")
    private RestHighLevelClient client;

    @Test
    void createIndex() throws Exception {
        CreateIndexRequest request = new CreateIndexRequest("my_index");
        Map<String, Object> mappings = new HashMap<>();
        Map<String, Object> properties = new HashMap<>();
        mappings.put("properties", properties);
        Map<String, Object> desc = new HashMap<>();
        properties.put("desc", desc);
        desc.put("analyzer", "ik_max_word");
        desc.put("type", "text");
        desc.put("search_analyzer", "ik_max_word");
        properties.put("desc", desc);
        request.mapping(mappings);
        CreateIndexResponse response = client.indices().create(request, RequestOptions.DEFAULT);
        System.out.println(response.index());
        System.out.println(response);
    }

    @Test
    public void testDeleteIndex() throws Exception {
        DeleteIndexRequest myIndex = new DeleteIndexRequest("my_index");
        AcknowledgedResponse delete = client.indices().delete(myIndex, RequestOptions.DEFAULT);
        System.out.println(delete.isAcknowledged());
    }

    @Test
    public void testGetIndex() throws Exception {
        GetIndexRequest request = new GetIndexRequest("my_index");
        GetIndexResponse response = client.indices().get(request, RequestOptions.DEFAULT);
        System.out.println(response);
    }

    //下面开始进行文档操作

    @Test
    public void testCreateDocument() throws Exception {
        News news = new News("1", "东风快递,使命必达");
        IndexRequest request = new IndexRequest("my_index");
        request.timeout("1s");
        request.id(news.getId());
        request.source(JSON.toJSONString(news), XContentType.JSON);
        client.index(request, RequestOptions.DEFAULT);
    }

    //判断一个文档是否存在
    @Test
    public void  testExists() throws Exception{
        GetRequest request = new GetRequest("my_index","1");
        //获取资源
        request.fetchSourceContext(new FetchSourceContext(false));
        //存储字段
        //request.storedFields("_none_");
        boolean response = client.exists(request, RequestOptions.DEFAULT);
        System.out.println(response);
        //System.out.println(response.isExists());

    }

    //获取文档
    @Test
    public void  testGetDocument() throws Exception{
        GetRequest request = new GetRequest("my_index", "1");
        GetResponse response = client.get(request, RequestOptions.DEFAULT);
        System.out.println(response.getSourceAsString());
        System.out.println(response);
    }

    //下面测试更新
    @Test
    public void  testUpdateDocument() throws Exception{
        UpdateRequest request = new UpdateRequest("my_index", "1");
        News news = new News("1", "东风快递,使命必达");
        request.doc(JSON.toJSONString(news),XContentType.JSON);
        //一般的请求都要设置超时时间
        request.timeout("1s");
        UpdateResponse response = client.update(request, RequestOptions.DEFAULT);
        System.out.println(response);
    }

    //删除文档
    @Test
    public void  testDeleteDocument() throws Exception{
        DeleteRequest request = new DeleteRequest("my_index", "1");
        request.timeout("1s");
        DeleteResponse delete = client.delete(request, RequestOptions.DEFAULT);
        System.out.println(delete.status());
    }

    //批量插入数据
    @Test
    public void  testBulk() throws Exception{
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.timeout("30s");
        List<News> newsList = new ArrayList<>();
        newsList.add(new News("1","东风快递,使命必达"));
        newsList.add(new News("2","以德服人,以理服人"));
        newsList.forEach((item)->{
            bulkRequest.add(new IndexRequest("my_index").id(item.getId()).source(JSON.toJSONString(item),XContentType.JSON));
        });
        BulkResponse bulk = client.bulk(bulkRequest, RequestOptions.DEFAULT);
        System.out.println(bulk.hasFailures());
    }
    //下面开始查询
    @Test
    public void  testGetOne() throws Exception{
        GetRequest request = new GetRequest("my_index", "1");
        GetResponse response = client.get(request, RequestOptions.DEFAULT);
        System.out.println(response.getSourceAsString());
    }
    //开始进行匹配查询

    @Test
    public void  testQuery() throws Exception{
        SearchRequest request = new SearchRequest("my_index");

        SearchSourceBuilder builder = new SearchSourceBuilder();

        MatchQueryBuilder query = QueryBuilders.matchQuery("id","1");
        builder.query(query);
        request.source(builder);
        SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            System.out.println(hit.getSourceAsString());
        }
    }


}
